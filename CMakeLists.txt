cmake_minimum_required( VERSION 2.8 )
enable_language( CXX )

project( pluma )
include_directories( "include" "src" )
file( GLOB src "src/Pluma/*.cpp" )
add_library( pluma SHARED ${src} )

install( DIRECTORY "include/Pluma" DESTINATION "include" )
install( TARGETS pluma DESTINATION "lib" )
