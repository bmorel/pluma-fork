#!/bin/sh
mkdir deb
rm deb/*
dpkg-deb -b all/pluma-doc ./deb
dpkg-deb -b all/pluma-dev ./deb
dpkg-deb -b all/pluma-example ./deb
dpkg-deb -b `dpkg --print-architecture`/pluma-dbg ./deb
dpkg-deb -b `dpkg --print-architecture`/pluma-bin ./deb
