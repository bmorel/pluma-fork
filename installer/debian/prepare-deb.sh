#!/bin/sh
cp -r ../../doc/html all/pluma-doc/usr/share/doc/pluma-doc/manual/
cp -r ../../include all/pluma-dev/usr/
#cp -r ../../example/build all/pluma-example/usr/share/doc/pluma-example/example/build
cp -r ../../example/src all/pluma-example/usr/share/doc/pluma-example/example/src

mkdir -p `dpkg --print-architecture`/pluma-bin/usr/lib/
mkdir -p `dpkg --print-architecture`/pluma-dbg/usr/lib/
cp -r ../../lib/libpluma.so `dpkg --print-architecture`/pluma-bin/usr/lib/
cp -r ../../lib/libpluma-d.so `dpkg --print-architecture`/pluma-dbg/usr/lib/
